﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Controls the starting and the ending of the race
/// </summary>
public class GameManager : MonoBehaviour{
	
	public TMPro.TextMeshProUGUI txtTime;
	public GameEvent startRace;

	private bool started = false;
	private bool ended = false;
	private float time;

	private float timeToRestart = 0, timeToNextStage = 0;


	public void Update() {
		//If you press Restart button for half second it restarts the actual level
		if (Input.GetButton("Restart")) {
			timeToRestart += Time.deltaTime;
			if (timeToRestart > 0.5f) {
				Restart();
			}
		}
		if (Input.GetButtonUp("Restart")) {
			timeToRestart = 0;
		}

		// Same as restart but when you end the race and you can go to the next lvl
		// If there is no next level it doesnt work
		if (Input.GetButton("Next")) {
			if (!ended)
				return;
			timeToNextStage += Time.deltaTime;
			if (timeToNextStage > 0.5f) {
				NextStage();
			}
		}
		if (Input.GetButtonUp("Restart")) {
			timeToNextStage = 0;
		}
	}

	/// <summary>
	/// Restart the actual level
	/// </summary>
	public void Restart() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	/// <summary>
	/// Checks if there is a next level and if does, load it
	/// </summary>
	public void NextStage() {
		int v = SceneManager.GetActiveScene().buildIndex+1;
		if (SceneManager.sceneCountInBuildSettings> v) {
			SceneManager.LoadScene(v);
		}
	}

	/// <summary>
	/// EVENT
	/// Starts the screen timer
	/// </summary>
	public void StartTime() {
		if (!started) {
			started = true;
			StartCoroutine(Timer());
		}
	}

	/// <summary>
	/// EVENT
	/// Ends the screen timer
	/// </summary>
	public void EndTime() {
		if (started) {
			started = false;
			ended = true;
			StartCoroutine(Timer());
		}
	}


	/// <summary>
	/// Increases the time by 10 miliseconds and updates the text
	/// </summary>
	IEnumerator Timer() {
		time += 1f;
		txtTime.text = Mathf.Floor(time / 60 / 60).ToString("00") + ":" + Mathf.Floor(time / 60 % 60).ToString("00") + ":" + (time % 60).ToString("00");
		yield return new WaitForSeconds(0.01f);

		if (started)
			StartCoroutine(Timer());
	}
}
