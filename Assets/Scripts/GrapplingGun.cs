﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System;
using UnityEngine;
using UnityEngine.UI;

public class GrapplingGun : MonoBehaviour{

	private LineRenderer lr;
	private Vector3 grapplePoint;
	private float maxGrapplingDistance = 100f;
	private SpringJoint joint;

	public LayerMask whatCanBeGrappled;
	public Transform gunTip, camera, player;
	public Image bg, fg;
	public Color red, green;

	Rigidbody rb;


	public void Awake() {
		lr = GetComponent<LineRenderer>();
        rb = player.GetComponent<Rigidbody>();
	}

    public void Update() {
		/*
		 * Sends a ray towards from the camera, if it collides within the range and its the appropiate layer 
		 * the crosshair changes to green, then you can grab it
		 * 
		 * Else if there is no place to grab it sets to red
		 */
		Ray ray = new Ray(camera.position, camera.forward);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, maxGrapplingDistance, whatCanBeGrappled)) {
			bg.color = fg.color = green;
			if (Input.GetMouseButton(0)&&!joint) {
				StartGrappling(hit);
			}
		} else 
			bg.color = fg.color = red;
		
		if (Input.GetMouseButtonUp(0)) {
            StopGrappling();
        }

		/*
		 * If you are grabbed to a point:
		 *  If it has a rigidbody updates the anchorpoint to the rigidbody
		 *  
		 *  also if you jump, stops the grappling and add you a force upwards
		 */
		if (joint) {
			if (joint.connectedBody != null) {
				grapplePoint = joint.connectedBody.position;
				joint.connectedAnchor = grapplePoint;
			}

			if (Input.GetButton("Jump")) {
				StopGrappling();
				rb.velocity = Vector3.zero;
				rb.AddForce(Vector2.up * 1000f * 1.5f);
			}
		}
    }

	/// <summary>
	/// Checks if you wanna go up or down the rope
	/// </summary>
    public void FixedUpdate() {
     	if (joint) {
			if (Input.GetButton("Recojer")) {
				PickUpRope();
			}
			if (Input.GetButton("Echar")) {
				PickDownRope();
			}
		}
	}

	/// <summary>
	/// If you press to go up it gets the vector towards the grapplePoint and add force towards 
	/// </summary>
	private void PickUpRope() {
		print("Recogiendo");
        //this.transform.parent.parent.parent.Translate(-(transform.position - grapplePoint) * 0.75f * Time.deltaTime);
        Vector3 dir = -(transform.position - grapplePoint).normalized * 200f;
        rb.AddForce(dir);

		RecalcDistances();
    }

	/// <summary>
	/// If you press to go down it gets the vector towards the grapplePoint and add force away from it 
	/// </summary>
	private void PickDownRope() {
		print("Desrecogiendo");
        //this.transform.parent.parent.parent.Translate((transform.position - grapplePoint) * 0.75f * Time.deltaTime);
        Vector3 dir = (transform.position - grapplePoint).normalized * 400f;
        rb.AddForce(dir);
		
		RecalcDistances();
    }

	public void LateUpdate() {
		DrawRope();
	}

	private void StopGrappling() {
		lr.positionCount = 0;
		Destroy(joint);
	}

	/// <summary>
	/// Gets the point and save it, if has a rigidbody adds it and sets the modifications
	/// (spring damper and mass) to the joint
	/// </summary>
	/// <param name="hit"></param>
	private void StartGrappling(RaycastHit hit) {
		grapplePoint = hit.point;
		joint = player.gameObject.AddComponent<SpringJoint>();
		joint.autoConfigureConnectedAnchor = false;
		joint.connectedAnchor = grapplePoint;
		if (hit.collider.GetComponent<HingeJoint>()) {
			joint.connectedBody = hit.collider.GetComponent<Rigidbody>();
		}
		joint.minDistance = 0.1f; ;

		RecalcDistances();

		joint.spring = 4.5f;
		joint.damper = 7f;
		joint.massScale = 4.5f;

		lr.positionCount = 2;
		
	}

    private void RecalcDistances() {
		float distance = 0;
		if (joint.connectedBody != null)
			distance = Vector3.Distance(player.position, joint.connectedBody.position);
		else
			distance = Vector3.Distance(player.position, grapplePoint);

		joint.maxDistance = distance * 0.8f; ;
	}

	void DrawRope() {
		if (!joint) return;

		lr.SetPosition(0, gunTip.position);
		lr.SetPosition(1, grapplePoint);
	}
}
