﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour{

	public Transform menus;

	public void Start() {
		ChangeMenu(0);
	}

	/// <summary>
	/// Carga la escena que le pases
	/// </summary>
	/// <param name="i">buildIndex de la escena</param>
	public void LoadScene(int i) {
		SceneManager.LoadScene(i);
	}


	public void ExitGame() {
		Application.Quit();
	}

	/// <summary>
	/// Cambia al menú en la posición que le digas
	/// </summary>
	/// <param name="menu">Indice del menú</param>
	public void ChangeMenu(int menu) {
		if (menu >= menus.childCount) return;
		for (int i = 0; i < menus.childCount; i++) {
			if (i == menu) {
				menus.GetChild(i).gameObject.SetActive(true);
				continue;
			}
			menus.GetChild(i).gameObject.SetActive(false);
		}
	}
}
