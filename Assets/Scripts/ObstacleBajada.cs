﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// It moves an object down to targetPos
/// </summary>
public class ObstacleBajada : MonoBehaviour{

	public float time;
	public Vector3 targetPos;

	private Vector3 startPos;
	private bool started;


	public void Start() {
		startPos = transform.position;
	}

	public void OnStart() {
		if (!started) {
			started = true;
			StartCoroutine(GoDown());
		}
	}

	/// <summary>
	/// It moves this object from the start position towards the target within the Time variable
	/// </summary>
	IEnumerator GoDown() {
		for(float t = 0f; t < 1f; t += Time.deltaTime/time) {
			transform.position = Vector3.Lerp(startPos, startPos + targetPos, t);
			yield return null;
		}
	}
}
