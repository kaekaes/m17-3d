﻿/*
* Copyright (c) AtheroX
* https://twitter.com/athero_x
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Gets it's childrens and rotates them towards the target
/// </summary>
public class SignManager : MonoBehaviour {

	public Transform target;
	public List<GameObject> list;

	void Start() {
		foreach (Transform c in transform) {
			list.Add(c.gameObject);
		}
		InvokeRepeating("SlowUpdate", 0.1f, 0.1f);
	}

	public void SlowUpdate() {
		foreach (GameObject s in list) {
			Transform t = s.transform;
			LookTowards(ref t, target);
		}

	}

	public static void LookTowards(ref Transform transform, Transform target) {
		Quaternion targetRotation = Quaternion.LookRotation(target.transform.position - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 25 * Time.deltaTime);
	}
}
