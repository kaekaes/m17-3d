Controles:
|     Control             | Moverse                        | Correr                                          | Agacharse                                 
|-------------------------|--------------------------------|-------------------------------------------------|-------------------------------------------
| Descripción del control | Mover personaje y girar cámara | Aumenta la velocidad, también afecta en el aire | Permite deslizarse pero no puedes moverte 
| Teclado y ratón         | WASD y ratón                   | Left shift                                      | Left control                              
| Joystick WIP            | Joystick izquierdo y derecho   | Cuadrado en PS  - X en Xbox                     | Círculo en PS - B en Xbox                 

| Saltar                      | Engancharse                                                                          | Ir al gancho                             |
|-----------------------------|--------------------------------------------------------------------------------------|------------------------------------------|
| Saltas .-.                  | Te enganchas cuando el cuadrado sale en verde y tengas el arma de engancharte, claro | Subes la cuerda cogiendo mucha velocidad |
| Space                       | Click izquierdo                                                                      | E                                        |
| Triangulo en Ps - Y en Xbox | R2 en PS - RT en Xbox                                                                | R1 en PS - RB en Xbox                    |


Risketos Mínims
- 3D
- Ha d’haver càmara 3D
- Ha d’haver com a mínim una font d’il·luminació complexa, i varies en general
- Ha d’haver moviment en les tres dimensions
- Pot ser un plataformes 3D. En aquest cas el moviment ha de ser lliure en 3D. No feu armes de projectils. NO es un FPS.
- S’ha de fer servir scriptable objects


Risketos Adicionales
- Iluminación y Post procesado muy compleja
- Materiales emisivos y Shader reflectivo
- Físicas con SpringJoints y vectores complejos
- Úso de cuaterniones
- Control con mando (bastante mal)
- Lightmap
- Un (UNA UNIDAD DE) uso de DOTS
